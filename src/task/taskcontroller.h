// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef TASKMODEL_H
#define TASKMODEL_H

#include <QObject>

#include <RuntimeManager/Task>

class TaskController: public QObject
{
    Q_OBJECT

public:
    explicit TaskController(QObject *parent = nullptr);
    ~TaskController() override = default;

    void setMaxRunningTime(int maxRuntime);
    int maxRunningTime() const;

public slots:
    void start(int interval, int priority, int maxRuntime);
    void stop();

private:
    RuntimeManager::Task m_task;
    int m_maxRunningTime = 1;
};

#endif // TASKMODEL_H
