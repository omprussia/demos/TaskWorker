// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "taskapp.h"

#include <RuntimeManager/RuntimeDispatcher>
#include <auroraapp.h>
#include <QDebug>
#include <QThread>
#include <QQuickView>
#include <QQmlContext>
#include "taskcontroller.h"

#include <QTimer>
#include <QDir>
#include <QTextStream>

namespace {
    using namespace RuntimeManager;

    const QString backroundTaskID = QStringLiteral("Writer");

    QString appDir()
    {
        return Aurora::Application::filesDir(false).path();
    }

    const QString workerFileName = QStringLiteral("worker.txt");
}

int sendCoordinate(GeoPositionManager *manager);

TaskApp::TaskApp(QScopedPointer<QGuiApplication> &&app, QObject *parent)
    : QObject(parent)
    , m_app(app.take())
    , m_networkReplyHandler(new NetworkReplyHandler)
    , m_geoPositionManager(new GeoPositionManager(backroundTaskID))
    , m_taskController(new TaskController(m_app.data()))
{
    QDir dir;
    if (!dir.exists(appDir())) {
        if (!dir.mkpath(appDir())) {
            qWarning("Directory %s is not created!!!", appDir().toLocal8Bit().constData());
        }
    }

    const auto path = QString("%1/%2").arg(appDir(), workerFileName);

    QFile file(path);
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        out << "\n*****\n" << "Start: " << QDateTime::currentDateTime().toString().toUtf8() << "\n";
        file.close();
    }
}

int TaskApp::execute()
{
    RuntimeDispatcher *dispatcher = RuntimeDispatcher::instance();

    dispatcher->onTaskStarted(backroundTaskID, [&](const QString &taskID) {
        qDebug() << "onTaskStarted";
        QEventLoop loop;

        sendCoordinate(m_geoPositionManager.data());

        m_networkReplyHandler->sendTestConnect(backroundTaskID);

        QTimer timer;
        timer.setSingleShot(true);
        connect( &timer, &QTimer::timeout, &loop, &QEventLoop::quit );
        timer.start(m_taskController->maxRunningTime());
        loop.exec();

        QCoreApplication::exit(EXIT_SUCCESS);
    });

    dispatcher->onApplicationStarted([this]() {
        qDebug() << "onApplicationStarted";

        const auto filePath = QString("%1/%2").arg(appDir(), workerFileName);

        QQuickView *view = Aurora::Application::createView();
        view->setSource(Aurora::Application::pathTo(QLatin1String("qml/TaskWorker.qml")));
        view->rootContext()->setContextProperty("taskController", m_taskController.data());
        view->rootContext()->setContextProperty("filePath", filePath);
        view->show();
    });

    return m_app->exec();
}

int sendCoordinate(GeoPositionManager *manager)
{
    const auto path = QString("%1/%2").arg(appDir(), workerFileName);

    QObject::connect(manager, &GeoPositionManager::lastKnownPositionChanged, [path](QGeoPositionInfo info) {
        QFile file(path);
        if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
            QTextStream out(&file);
            out << QString("Position changed:")
                << QString("{'latitude': %1, 'longitude': %2}")
                   .arg(info.coordinate().latitude())
                   .arg(info.coordinate().longitude())
                << '\n';
            file.close();
        }
    });
    const QGeoPositionInfo &geoInfo= manager->lastKnownPosition();
    QGeoCoordinate lastCoordinate = geoInfo.coordinate();

    if (!geoInfo.isValid()) {
        qWarning() << "GEO INFO NOT VALID";
        qDebug() << "Set test data";
        lastCoordinate.setLatitude(0.127);
        lastCoordinate.setLongitude(0.08);
    }

    QFile file(path);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        QTextStream out(&file);
        out << "\n*****\n"
            << QString("Sending coordinate:")
            << QString("{'latitude': %1, 'longitude': %2}")
               .arg(lastCoordinate.latitude())
               .arg(lastCoordinate.longitude())
            << '\n';
        file.close();
    }

    qInfo() << QString("Sending coordinate:")
            << QString("{'latitude': %1, 'longitude': %2}")
               .arg(lastCoordinate.latitude())
               .arg(lastCoordinate.longitude());

    return 0;
}
