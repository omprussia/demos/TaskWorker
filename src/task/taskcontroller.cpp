// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "taskcontroller.h"

#include <QJsonObject>
#include <QJsonDocument>

#include <QDebug>

namespace  {
    using namespace RuntimeManager;

    const QString taskID = QStringLiteral("Writer");
}

TaskController::TaskController(QObject *parent)
    : QObject(parent)
    , m_task(taskID)
{

}

void TaskController::setMaxRunningTime(int maxRuntime)
{
    if (m_maxRunningTime != maxRuntime)
        m_maxRunningTime = maxRuntime;
}

int TaskController::maxRunningTime() const
{
    return m_maxRunningTime;
}

void TaskController::start(int interval, int priority, int maxRuntime)
{
    setMaxRunningTime(maxRuntime);

    m_task.withArguments({"--periodic"})
            .withInterval(interval * 60)
            .withPriority(priority)
            .withMaximumRunningTime(maxRuntime > 0 ? maxRuntime : 1);

    m_task.start([](const Error &err){
        qWarning() << "[Error] code = " << err.code()
                   << " message = " << err.message()
                   << " is = " << err.isError();
    });
}

void TaskController::stop()
{
    m_task.stop();
}
