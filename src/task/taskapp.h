// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef TASK_APP_H
#define TASK_APP_H

#include <QObject>
#include <QScopedPointer>
#include <QGuiApplication>

#include <memory>
#include "../geo/geopositionmanager.h"
#include "../network/networkreplyhandler.h"

#include "taskcontroller.h"

class TaskApp : public QObject
{
    Q_OBJECT
public:
    explicit TaskApp(QScopedPointer<QGuiApplication> &&app, QObject *parent = nullptr);
    ~TaskApp() override = default;

    int execute();

private:
    Q_DISABLE_COPY(TaskApp)

    QScopedPointer<QGuiApplication> m_app;
    QScopedPointer<NetworkReplyHandler> m_networkReplyHandler;
    QScopedPointer<GeoPositionManager> m_geoPositionManager;
    QScopedPointer<TaskController> m_taskController;
};

#endif // TASK_APP
