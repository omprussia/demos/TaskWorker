// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "geopositionmanager.h"

GeoPositionManager::GeoPositionManager(QString identifier, QObject *parent)
    : QObject(parent)
    , m_source(nullptr)
    , m_lastKnownPosition(QGeoPositionInfo())
    , m_identifier(identifier)
{
    qDebug() << "GeoPositionManager created in" << m_identifier;
    m_source = QGeoPositionInfoSource::createDefaultSource(this);
    if (m_source) {
        m_lastKnownPosition = m_source->lastKnownPosition(false);
        qDebug() << m_lastKnownPosition.isValid();

        connect(m_source, &QGeoPositionInfoSource::positionUpdated, this, &GeoPositionManager::onPositionUpdated);

        m_source->setUpdateInterval(m_source->minimumUpdateInterval());
        m_source->startUpdates();
    }
}

QGeoPositionInfo GeoPositionManager::lastKnownPosition() const
{
    return m_lastKnownPosition;
}

void GeoPositionManager::onPositionUpdated(const QGeoPositionInfo &info)
{
    qDebug() << "QGeoPositionInfoSource::positionUpdated in" << m_identifier;
    m_lastKnownPosition = info;
    emit lastKnownPositionChanged(m_lastKnownPosition);
}
