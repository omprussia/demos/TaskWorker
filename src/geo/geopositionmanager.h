// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef GEOPOSITIONMANAGER_H
#define GEOPOSITIONMANAGER_H

#include <QDebug>
#include <QtPositioning/QGeoPositionInfo>
#include <QtPositioning/QGeoPositionInfoSource>

class GeoPositionManager : public QObject {
    Q_OBJECT
public:
    GeoPositionManager(QString identifier, QObject *parent = nullptr);

    ~GeoPositionManager() override = default;

    QGeoPositionInfo lastKnownPosition() const;

private slots:
    void onPositionUpdated(const QGeoPositionInfo &info);

signals:
    void lastKnownPositionChanged(QGeoPositionInfo);

private:
    QGeoPositionInfoSource *m_source;
    QGeoPositionInfo m_lastKnownPosition;
    QString m_identifier;
};

#endif // GEOPOSITIONMANAGER_H
