// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "networkreplyhandler.h"

namespace
{
    const auto baseUrl = QStringLiteral("http://httpbin.org/get");
}

void NetworkReplyHandler::sendTestConnect(QString identifier)
{
    if(identifier != m_identifier)
        m_identifier = identifier;

    const QUrl url(baseUrl);
    QNetworkRequest request(url);

    request.setRawHeader("Content-Type", "application/json");

    qInfo() << QString("Sending TEST request 'GET %1' in %2").arg(baseUrl, m_identifier);

    QNetworkReply *reply = m_networkAccessManager.get(request);

    QObject::connect(reply, &QNetworkReply::finished, this, &NetworkReplyHandler::dataReady);
}

void NetworkReplyHandler::dataReady()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if (reply) {
        qInfo() << "TEST RESPONSE in " << m_identifier << ": error=" << reply->error();
        qInfo() << reply->readAll();
        reply->deleteLater();
    }
}
