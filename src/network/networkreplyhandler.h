// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NETWORKREPLYHANDLER_H
#define NETWORKREPLYHANDLER_H

#include <QNetworkReply>

class NetworkReplyHandler : public QObject {
    Q_OBJECT

public:
    void sendTestConnect(QString identifier);

private slots:
    void dataReady();

private:
    QNetworkAccessManager m_networkAccessManager;
    QString m_identifier;
};
#endif // NETWORKREPLYHANDLER_H
