[RU](README.ru.md) | [**EN**](README.md)

## Task Worker

## Description  

The application allows you to run additional processes in the background. This can be useful, for example, 
for performing data backups, software updates, sending REST requests, monitoring hardware status, 
and other tasks that require continuous execution.
A background task is created using the Task class from [RuntimeManager](https://developer.auroraos.ru/doc/5.1.3/software_development/reference/runtime_manager).
For more information, please refer to [documentation](https://developer.auroraos.ru/doc/5.1.3/software_development/reference/runtime_manager/background_tasks).

## Build status:
- example - [![pipeline status](https://gitlab.com/omprussia/demos/TaskWorker/badges/example/pipeline.svg)](https://gitlab.com/omprussia/demos/TaskWorker/-/commits/example)
- dev - [![pipeline status](https://gitlab.com/omprussia/demos/TaskWorker/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/demos/TaskWorker/-/commits/dev)

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Compatibility](#compatibility)
- [Build features](#build-features)
- [Branch info](#branch-info)
- [Install and Build](#install-and-build)
- [Screenshots](#screenshots)
- [Usage options](#usage-options)
- [Project Structure](#project-structure)
- [Terms of Use and Participation in Development](#terms-of-use-and-participation-in-development)

<a name=“Compatibility”></a>
## Compatibility 
The application works correctly starting with Aurora OS 5.x.x and higher.

<a name=“Build-features”></a>
## Build features
The project is built via the CMakeLists.txt file. The packages required for RuntimeManager are installed together with the application as dependencies.  

<a name="Branch-info"></a>
## Branch info
[Branches](https://developer.auroraos.ru/doc/5.1.3/software_development/examples#branches)

<a name=“Install-and-build”></a>
## Install and Build
The project is built in the usual way using the Aurora SDK. [Documentation](https://developer.auroraos.ru/doc/5.1.3/sdk/app_development/work/build) to build.  

<a name="Screenshots"></a>
## Screenshots
![Screenshots](screenshots/screenshots.png)

<a name=“Usage-options”></a>
## Usage options 
* Create a background task and after a time (15 minutes), get the current coordinates of the device.
* Send a GET request to https://httpbin.org/get, and output the response to the application console after a time(15 minutes).  
In both cases, after the background task is started, the application can be closed.

<a name=“Project-structure”></a>
## Project Structure
The project has a standard C++ and QML based application structure for the Aurora OS.

* **[CMakeLists.txt](CMakeLists.txt)** the file describes the project structure for the cmake build system.
* **[icons](icons)** the directory contains application icons for supported screen resolutions.
* **[qml](qml)** the directory contains QML source code and user interface resources.
  * **[cover](qml/cover)** the directory contains implementations of application covers.
  * **[images](qml/images)** the directory contains additional user interface icons.
  * **[pages](qml/pages)** the directory contains the application pages.
  * **[TaskWorker.qml](qml/TaskWorker.qml)** the file provides the application window implementation.
* **[rpm](rpm)** the directory contains the rpm package build settings.
  * **[ru.auroraos.TaskWorker.spec](rpm/ru.auroraos.TaskWorker.spec)** the file is used by the rpmbuild tool.
* **[src](src)** the directory contains C++ source code.
  * **[geo](src/geo)** the directory contains a manager for working with geolocation.
  * **[network](src/network)** the directory contains a request handler.
  * **[task](src/task)** the directory contains the background task controller.
  * **[main.cpp](src/main.cpp)** the file is the entry point to the application.
* **[translations](translations)** the directory contains the user interface translation files.
* **[ru.auroraos.TaskWorker.desktop](ru.auroraos.TaskWorker.desktop)** the file defines the display and startup options for the application.

<a name=“Terms-of-use-and-participation-in-development”></a>
## Terms of Use and Participation in Development 
The project source code is provided under [license](LICENSE.BSD-3-Clause.md), which allows its use in third-party applications.

The [Participant Agreement](CONTRIBUTING.md) governs the rights granted by the participants to the Open Mobile Platform Company.

Participant information is listed in the [AUTHORS](AUTHORS.md) file.

The [Code of Conduct](CODE_OF_CONDUCT.md) is the current set of rules of the Open Mobile Platform Company, which communicates the 
expectations for interaction between community members when communicating and working on projects.
