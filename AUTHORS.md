# Authors

* Alberto Mardegan, <a.mardegan@omp.ru>
  * Developer 2024
* Andrej Begichev, <a.begichev@omp.ru>
  * Maintainer, 2024
  * Developer, 2024
* Dmitry Lapshin, <d.lapshin@omp.ru>
  * Reviewer, 2024
* Evgeniy Samohin, <e.samohin@omp.ru>
  * Reviewer, 2024
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
* Maksim Kosterin
  * Developer, 2024  
