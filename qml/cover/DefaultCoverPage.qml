// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    objectName: "defaultCover"

    CoverTemplate {
        objectName: "applicationCover"
        secondaryText: qsTr("TaskWorker")
        icon {
            source: Qt.resolvedUrl("../images/TaskWorker.svg")
            sourceSize {
                width: icon.width
                height: icon.height
            }
        }
    }
}
