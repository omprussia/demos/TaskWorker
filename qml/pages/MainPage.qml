// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: mainPage

    objectName: "mainPage"
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: flick

        anchors.fill: parent
        Column {
            width: parent.width
            spacing: Theme.paddingSmall

            PageHeader {
                title: qsTr("Task Worker")
            }

            SectionHeader {
                text: qsTr("Interval")
            }

            TextField {
                id: intervalField

                inputMethodHints: Qt.ImhDigitsOnly

                validator: IntValidator {
                    bottom: 15
                }

                placeholderText: qsTr("Interval in minutes(minimum 15)")

                text: "15"

                onAcceptableInputChanged: {
                    const buttonState = Number(text) > 14;
                    buttonStart.enabled = buttonState && !buttonStop.enabled;
                    buttonStop.enabled = buttonState && !buttonStart.enabled;
                }
            }

            SectionHeader {
                text: qsTr("Priority")
            }

            TextField {
                id: priorityField
                inputMethodHints: Qt.ImhDigitsOnly

                placeholderText: qsTr("0-19 (19 = lowest)")

                text: "1"
            }

            SectionHeader {
                text: qsTr("Max run time")
            }

            TextField {
                id: maxRuntimeField
                inputMethodHints: Qt.ImhDigitsOnly

                placeholderText: qsTr("Runtime, in seconds")

                text: "60"
            }

            ButtonLayout {
                width: parent.width

                anchors.horizontalCenter: parent.horizontalCenter

                Button {
                    id: buttonStart

                    text: qsTr("Start")
                    onClicked: {
                        buttonStart.enabled = false;
                        buttonStop.enabled = true;
                        const interval = Number(intervalField.text);
                        const priority = Number(priorityField.text);
                        const maxRuntime = Number(maxRuntimeField.text);
                        taskController.start(interval, priority, maxRuntime);
                    }
                }

                Button {
                    id: buttonStop

                    enabled: false

                    text: qsTr("Stop");
                    onClicked: {
                        buttonStart.enabled = true;
                        buttonStop.enabled = false;
                        taskController.stop();
                    }
                }
            }

            Label {
                anchors {
                    left: parent.left;
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }

                text: qsTr("The background task writes to <a href=\"file:/%1\">%1</a>").arg(filePath)
                textFormat: Text.StyledText
                wrapMode: Text.Wrap
                linkColor: Theme.primaryColor
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
    }
}
