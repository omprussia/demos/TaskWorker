<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../../TaskWorker/qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>TaskWorker</source>
        <translation>Рабочий</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="22"/>
        <source>Task Worker</source>
        <translation>Рабочий</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="26"/>
        <source>Interval</source>
        <translation>Интервал</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="38"/>
        <source>Interval in minutes(minimum 15)</source>
        <translation>Интервал в минутах(минимум 15)</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="50"/>
        <source>Priority</source>
        <translation>Приоритет</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="57"/>
        <source>0-19 (19 = lowest)</source>
        <translation>0-19 (19 = низкий)</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="63"/>
        <source>Max run time</source>
        <translation>Макс. количество раз</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="70"/>
        <source>Runtime, in seconds</source>
        <translation>Количество раз, в секундах</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="83"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="99"/>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="116"/>
        <source>The background task writes to &lt;a href=&quot;file:/%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Фоновая задача записывается в &lt;a href=&quot;file:/%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
</context>
</TS>
