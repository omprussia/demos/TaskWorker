<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../../TaskWorker/qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>TaskWorker</source>
        <translation>TaskWorker</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="22"/>
        <source>Task Worker</source>
        <translation>Task Worker</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="26"/>
        <source>Interval</source>
        <translation>Interval</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="38"/>
        <source>Interval in minutes(minimum 15)</source>
        <translation>Interval in minutes(minimum 15)</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="50"/>
        <source>Priority</source>
        <translation>Priority</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="57"/>
        <source>0-19 (19 = lowest)</source>
        <translation>0-19 (19 = lowest)</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="63"/>
        <source>Max run time</source>
        <translation>Max run time</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="70"/>
        <source>Runtime, in seconds</source>
        <translation>Runtime, in seconds</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="83"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="99"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../../TaskWorker/qml/pages/MainPage.qml" line="116"/>
        <source>The background task writes to &lt;a href=&quot;file:/%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>The background task writes to &lt;a href=&quot;file:/%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
</context>
</TS>
